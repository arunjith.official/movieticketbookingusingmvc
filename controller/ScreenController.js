import Screen from "../model/screen.js";
let screen1 = new Screen ();
class ScreenController {
    constructor() {
    }
    bookOneSeat (seatNumber) {
        const selectedSeat=screen1.getSeats()[seatNumber];
        if (selectedSeat.booked===false) {
            let bookingStatus=screen1.bookSeat(seatNumber);
            return bookingStatus
        }
        else {
            return false
        }
    }
    cancelOneSeat (seatNumber) {
        const selectedSeat=screen1.getSeats()[seatNumber];
        if (selectedSeat.booked===true) {
            let cancellationStatus=screen1.cancelSeat(seatNumber);
            return cancellationStatus
        }
        else {
            return false
        }
    }
    getSeats(){
        return screen1.getSeats();
    }
    
}
export default ScreenController