class Screen {
    constructor () {
        this.seats=[
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false},
            {booked: false}
        ]
    }
    bookSeat (seatNumber) {
        this.seats[seatNumber].booked=true
        return true
    }
    cancelSeat (seatNumber) {
        this.seats[seatNumber].booked=false
    }
    getSeats() {
        return this.seats;
    }
}
export default Screen